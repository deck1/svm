# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from numpy import arange
from sklearn import tree
from fpdf import FPDF
# import pickle
import datetime
from datetime import datetime as dt
from datetime import timedelta
# import gc

from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.model_selection import cross_validate
from sklearn import svm
from sklearn.decomposition import PCA as RandomizedPCA
from sklearn.pipeline import make_pipeline
import boto3
import io
import pickle5 as pickle

# %%
s3 = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com")

print("----load start------")
dfone=s3.Object('upload-zszljfwd','loan_f10.pickle').get()['Body'].read()
df=pd.read_pickle(io.BytesIO(dfone),compression=None)
print("----loaded done------")
# %%
print(df.shape)

# %%
df=df.drop(columns=['AA036_EXT','AA037_EXT','AA038_EXT','AA039_EXT','AA040_EXT','AA041_EXT','AA042_EXT','AA043_EXT','AA044_EXT','AA045_EXT','AA046_EXT','AA047_EXT','AA048_EXT','AA049_EXT','AA050_EXT'])

# %%
df=df.drop(columns=['LOAN_NAME_I044_EXT','LOAN_NAME_I045_EXT','LOAN_NAME_I046_EXT','LOAN_NAME_I047_EXT','LOAN_NAME_I048_EXT','LOAN_NAME_I049_EXT','LOAN_NAME_I050_EXT','LOAN_NAME_I051_EXT','LOAN_NAME_I052_EXT','LOAN_NAME_I053_EXT','LOAN_NAME_I054_EXT','LOAN_NAME_I055_EXT','LOAN_NAME_I056_EXT','LOAN_NAME_I057_EXT','LOAN_NAME_I058_EXT','LOAN_NAME_I059_EXT','LOAN_NAME_I060_EXT','LOAN_NAME_I061_EXT','LOAN_NAME_I062_EXT','LOAN_NAME_I063_EXT','LOAN_NAME_I064_EXT','LOAN_NAME_I065_EXT','LOAN_NAME_I066_EXT','LOAN_NAME_I067_EXT','LOAN_NAME_I068_EXT','LOAN_NAME_I069_EXT','LOAN_NAME_I070_EXT','LOAN_NAME_I071_EXT','LOAN_NAME_I072_EXT','LOAN_NAME_I073_EXT','LOAN_NAME_I074_EXT','LOAN_NAME_I075_EXT','LOAN_NAME_I076_EXT','LOAN_NAME_I077_EXT','LOAN_NAME_I078_EXT','LOAN_NAME_I079_EXT','LOAN_NAME_I080_EXT','LOAN_NAME_I081_EXT','LOAN_NAME_I082_EXT','LOAN_NAME_I083_EXT','LOAN_NAME_I084_EXT','LOAN_NAME_I085_EXT','LOAN_NAME_I086_EXT','LOAN_NAME_I087_EXT','LOAN_NAME_I088_EXT'])

# %%
df=df.drop(columns=['GENDER_I04_EXT','GENDER_I05_EXT','LOAN_TYPE_I07_EXT','LOAN_TYPE_I08_EXT','LOAN_TYPE_I09_EXT','LOAN_TYPE_I10_EXT','LOAN_TYPE_I11_EXT','LOAN_TYPE_I12_EXT','LOAN_TYPE_I13_EXT','LOAN_TYPE_I14_EXT','LOAN_TYPE_I15_EXT','LOAN_TYPE_I16_EXT','LOAN_TYPE_I17_EXT','LOAN_TYPE_I18_EXT','LOAN_TYPE_I19_EXT','LOAN_TYPE_I20_EXT','MORTAGE_TYPE_I06_EXT','MORTAGE_TYPE_I07_EXT','MORTAGE_TYPE_I08_EXT','MORTAGE_TYPE_I09_EXT','MORTAGE_TYPE_I10_EXT','MS_I04_EXT','MS_I05_EXT'])

# %%

# %%
sm = SMOTE()

# %%
"""
# Model
"""

# %%
# frames=[df1,df3,df4,df5]
# result=pd.concat(frames)

# %%
result=df

# %%
X_M2 = result.drop('ZZ_STATUS', axis=1)
y_M2 = result['ZZ_STATUS'].values

# %%
X_train_M2, X_test_M2, y_train_M2, y_test_M2 = train_test_split(X_M2,y_M2,test_size=0.20, random_state=0)
X_train_M2, X_val_M2, y_train_M2, y_val_M2 = train_test_split(X_train_M2,y_train_M2,test_size=0.25, random_state=0)

# %%
print("---- smote ------")
X_train_smote_M2, y_train_smote_M2 = sm.fit_resample(X_train_M2, y_train_M2)
print("---- smote end ------")
# %%
print("---- model defination ----")
model = svm.NuSVC(
                nu=0.5,
                kernel='rbf', 
                gamma='scale', 
                coef0=0.0, 
                shrinking=True, 
                probability=True, 
                tol=0.001, 
                cache_size=8048, 
                class_weight=None, 
                verbose=True, 
                max_iter=5, 
                decision_function_shape='ovr', 
                break_ties=False, 
                random_state=None)

# %%
print("model fit")
model.fit(X_train_smote_M2, y_train_smote_M2)
print("model fit finish")
# %%
def model_matrix(newmodel,X_new,y_new,type):
    predicted = newmodel.predict(X_new)
    precision=precision_score(y_new, predicted.round(), average='weighted')
    recall=recall_score(y_new, predicted.round(), average='weighted')
    f1=f1_score(y_new, predicted.round(), average='weighted')
    accuracy=accuracy_score(y_new, predicted.round())
    CM = confusion_matrix(y_new, predicted.round())
    (TN,FN,TP,FP) = (CM[0][0],CM[1][0],CM[1][1],CM[0][1])
    FPR = FP/(FP+TN)
    print(type)
    print('recall : ' + str(recall))
    print('precision : ' + str(precision))
    print('Accuracy : ' + str(accuracy))
    print('FPR : ' + str(FPR))
    print('F1 : ' + str(f1))  

# %%
model_matrix(model,X_test_M2,y_test_M2,'test')

# %%
